use {anyhow::Result, std::io::Read};

#[derive(Debug)]
pub struct Texture {
    pub texture: wgpu::Texture,
    pub view: wgpu::TextureView,
    pub sampler: wgpu::Sampler,
}

#[derive(Debug)]
pub struct Skin {
    pub width: u32,
    pub height: u32,
    pub texture: Texture,
}

impl Skin {
    pub fn create<R>(src: R, device: &wgpu::Device, queue: &wgpu::Queue) -> Result<Self>
    where
        R: Read,
    {
        let decoder = png::Decoder::new(src);
        let (info, mut reader) = decoder.read_info()?;
        let mut texels = vec![0; info.buffer_size()];
        reader.next_frame(&mut texels)?;

        let extent = wgpu::Extent3d {
            width: info.width,
            height: info.height,
            depth: 1,
        };

        let texture = device.create_texture(&wgpu::TextureDescriptor {
            label: Some("Skin Texture"),
            size: extent,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba8UnormSrgb,
            usage: wgpu::TextureUsage::SAMPLED | wgpu::TextureUsage::COPY_DST,
        });
        let view = texture.create_view(&wgpu::TextureViewDescriptor {
            label: Some("Skin Texture View"),
            ..wgpu::TextureViewDescriptor::default()
        });

        queue.write_texture(
            wgpu::TextureCopyView {
                texture: &texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
            },
            &texels,
            wgpu::TextureDataLayout {
                offset: 0,
                bytes_per_row: 4 * info.width,
                rows_per_image: 0,
            },
            extent,
        );

        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            label: Some("Skin Texture Sampler"),
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });

        Ok(Self {
            width: info.width,
            height: info.height,
            texture: Texture {
                texture,
                view,
                sampler,
            },
        })
    }
}

pub fn create_depth_texture(
    device: &wgpu::Device,
    swap_chain_desc: &wgpu::SwapChainDescriptor,
) -> Texture {
    let extent = wgpu::Extent3d {
        width: swap_chain_desc.width,
        height: swap_chain_desc.height,
        depth: 1,
    };

    let texture = device.create_texture(&wgpu::TextureDescriptor {
        label: Some("Depth Texture"),
        size: extent,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Depth32Float,
        usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT | wgpu::TextureUsage::SAMPLED,
    });
    let view = texture.create_view(&wgpu::TextureViewDescriptor {
        label: Some("Depth Texture View"),
        ..wgpu::TextureViewDescriptor::default()
    });
    let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
        label: Some("Depth Texture Sampler"),
        address_mode_u: wgpu::AddressMode::ClampToEdge,
        address_mode_v: wgpu::AddressMode::ClampToEdge,
        address_mode_w: wgpu::AddressMode::ClampToEdge,
        mag_filter: wgpu::FilterMode::Linear,
        min_filter: wgpu::FilterMode::Linear,
        mipmap_filter: wgpu::FilterMode::Nearest,
        compare: Some(wgpu::CompareFunction::LessEqual),
        lod_min_clamp: -100.0,
        lod_max_clamp: 100.0,
        ..Default::default()
    });

    Texture {
        texture,
        view,
        sampler,
    }
}
