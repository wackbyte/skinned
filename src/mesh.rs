use {crate::vertex::Vertex, wgpu::util::DeviceExt};

#[derive(Debug, Default, Clone)]
pub struct MeshDescriptor<'a> {
    pub vertex_data: &'a [Vertex],
    pub index_data: &'a [u16],
    pub vertex_buf_label: Option<&'a str>,
    pub index_buf_label: Option<&'a str>,
}

#[derive(Debug)]
pub struct Mesh {
    pub vertex_buf: wgpu::Buffer,
    pub index_buf: wgpu::Buffer,
    pub index_count: usize,
}

impl Mesh {
    pub fn create(device: &wgpu::Device, desc: &MeshDescriptor) -> Self {
        Self {
            vertex_buf: device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: desc.vertex_buf_label,
                contents: bytemuck::cast_slice(desc.vertex_data),
                usage: wgpu::BufferUsage::VERTEX,
            }),
            index_buf: device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: desc.index_buf_label,
                contents: bytemuck::cast_slice(desc.index_data),
                usage: wgpu::BufferUsage::INDEX,
            }),
            index_count: desc.index_data.len(),
        }
    }
}
