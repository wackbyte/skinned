#version 450

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec2 a_TexCoord;
layout(location = 2) in mat4 a_Transform;
layout(location = 0) out vec2 v_TexCoord;

layout(set = 1, binding = 0) uniform Uniforms {
    mat4 u_Camera;
};

void main() {
    v_TexCoord = a_TexCoord;
    gl_Position = u_Camera * a_Transform * vec4(a_Position, 1.0);
}
