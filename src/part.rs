use {
    crate::{
        instance::Instance,
        mesh::{Mesh, MeshDescriptor},
        vertex::Vertex,
    },
    cgmath::{prelude::*, Matrix4, Vector2, Vector3},
    wgpu::util::DeviceExt,
};

const INDEX_DATA: [u16; 36] = [
    0, 1, 2, 2, 3, 0, // top
    4, 5, 6, 6, 7, 4, // bottom
    8, 9, 10, 10, 11, 8, // right
    12, 13, 14, 14, 15, 12, // left
    16, 17, 18, 18, 19, 16, // front
    20, 21, 22, 22, 23, 20, // back
];

#[derive(Debug, Clone)]
pub struct PartDescriptor<'a> {
    pub label: Option<&'a str>,
    pub instance: Instance,
    pub extent: Vector3<u32>,
    pub texture_offset: Vector2<u32>,
    pub texture_size: Vector2<u32>,
}

#[derive(Debug)]
pub struct Part {
    pub mesh: Mesh,
    pub instance_buf: wgpu::Buffer,
}

impl Part {
    pub fn create(device: &wgpu::Device, desc: &PartDescriptor) -> Self {
        let x = desc.extent.x as f32;
        let x2 = x / 2.0;
        let y = desc.extent.y as f32;
        let y2 = y / 2.0;
        let z = desc.extent.z as f32;
        let z2 = z / 2.0;
        let tx = desc.texture_offset.x as f32;
        let ty = desc.texture_offset.y as f32;
        let tw = desc.texture_size.x as f32;
        let th = desc.texture_size.y as f32;

        // TODO: just use normalized floats
        let normalize = move |tex_coord: [f32; 2]| [tex_coord[0] / tw, tex_coord[1] / th];

        #[rustfmt::skip]
        let vertex_data = [
            // top (0, 0, 1)
            Vertex::new([-x2, -y2, z2], normalize([tx + x + y, ty])), // top-right
            Vertex::new([x2, -y2, z2], normalize([tx + y, ty])), // top-left
            Vertex::new([x2, y2, z2], normalize([tx + y, ty + y])), // bottom-left
            Vertex::new([-x2, y2, z2], normalize([tx + x + y, ty + y])), // bottom-right
            // bottom (0, 0, -1)
            Vertex::new([-x2, y2, -z2], normalize([tx + x * 2.0 + y, ty])), // top-right
            Vertex::new([x2, y2, -z2], normalize([tx + x + y, ty])), // top-left
            Vertex::new([x2, -y2, -z2], normalize([tx + x + y, ty + y])), // bottom-left
            Vertex::new([-x2, -y2, -z2], normalize([tx + x * 2.0 + y, ty + y])), // bottom-right
            // right (1, 0, 0)
            Vertex::new([x2, -y2, -z2], normalize([tx, ty + y + z])), // bottom-left
            Vertex::new([x2, y2, -z2], normalize([tx + y, ty + y + z])), // bottom-right
            Vertex::new([x2, y2, z2], normalize([tx + y, ty + y])), // top-right
            Vertex::new([x2, -y2, z2], normalize([tx, ty + y])), // top-left
            // left (-1, 0, 0)
            Vertex::new([-x2, -y2, z2], normalize([tx + x + y * 2.0, ty + y])), // top-right
            Vertex::new([-x2, y2, z2], normalize([tx + x + y, ty + y])), // top-left
            Vertex::new([-x2, y2, -z2], normalize([tx + x + y, ty + y + z])), // bottom-left
            Vertex::new([-x2, -y2, -z2], normalize([tx + x + y * 2.0, ty + y + z])), // bottom-right
            // front (0, 1, 0)
            Vertex::new([x2, y2, -z2], normalize([tx + y, ty + y + z])), // bottom-left
            Vertex::new([-x2, y2, -z2], normalize([tx + x + y, ty + y + z])), // bottom-right
            Vertex::new([-x2, y2, z2], normalize([tx + x + y, ty + y])), // top-right
            Vertex::new([x2, y2, z2], normalize([tx + y, ty + y])), // top-left
            // back (0, -1, 0)
            Vertex::new([x2, -y2, z2], normalize([tx + x * 2.0 + y * 2.0, ty + y])), // top-right
            Vertex::new([-x2, -y2, z2], normalize([tx + x + y * 2.0, ty + y])), // top-left
            Vertex::new([-x2, -y2, -z2], normalize([tx + x + y * 2.0, ty + y + z])), // bottom-left
            Vertex::new([x2, -y2, -z2], normalize([tx + x * 2.0 + y * 2.0, ty + y + z])), // bottom-right
        ];
        let vertex_buf_label = desc.label.map(|label| format!("{} Vertex Buffer", label));
        let index_buf_label = desc.label.map(|label| format!("{} Index Buffer", label));
        let mesh_desc = MeshDescriptor {
            vertex_data: &vertex_data,
            index_data: &INDEX_DATA,
            vertex_buf_label: vertex_buf_label.as_deref(),
            index_buf_label: index_buf_label.as_deref(),
        };
        let mesh = Mesh::create(device, &mesh_desc);

        let instance_data: &[f32; 16] = desc.instance.transform.as_ref();
        let instance_buf_label = desc.label.map(|label| format!("{} Instance Buffer", label));
        let instance_buf = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: instance_buf_label.as_deref(),
            contents: bytemuck::cast_slice(instance_data),
            usage: wgpu::BufferUsage::VERTEX,
        });

        Self { mesh, instance_buf }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum BodyKind {
    /// 4-pixel wide arms.
    Classic,
    /// 3-pixel wide arms.
    Slim,
}

impl Default for BodyKind {
    fn default() -> Self {
        Self::Classic
    }
}

#[derive(Debug, Default, Clone)]
pub struct BodyDescriptor<'a> {
    pub label: Option<&'a str>,
    pub transform: Option<Matrix4<f32>>,
    pub kind: BodyKind,
}

#[derive(Debug)]
pub struct Body {
    // base parts
    pub head: Part,
    pub torso: Part,
    pub right_arm: Part,
    pub left_arm: Part,
    pub right_leg: Part,
    pub left_leg: Part,

    // overlay parts
    pub hat: Part,
    pub jacket: Part,
    pub right_sleeve: Part,
    pub left_sleeve: Part,
    pub right_pants_leg: Part,
    pub left_pants_leg: Part,
}

impl Body {
    pub fn create(device: &wgpu::Device, desc: &BodyDescriptor) -> Self {
        let transform = desc.transform.unwrap_or(Transform::one());

        let texture_size = Vector2::new(64, 64);

        let head_translation = Matrix4::from_translation(Vector3::new(0.0, 0.0, 10.0));
        let head_extent = Vector3::new(8, 8, 8);
        let torso_translation: Matrix4<f32> = Transform::one();
        let torso_extent = Vector3::new(8, 4, 12);
        let (right_arm_translation, left_arm_translation, arm_extent) = match desc.kind {
            BodyKind::Classic => (
                Matrix4::from_translation(Vector3::new(6.0, 0.0, 0.0)),
                Matrix4::from_translation(Vector3::new(-6.0, 0.0, 0.0)),
                Vector3::new(4, 4, 12),
            ),
            BodyKind::Slim => (
                Matrix4::from_translation(Vector3::new(5.5, 0.0, 0.0)),
                Matrix4::from_translation(Vector3::new(-5.5, 0.0, 0.0)),
                Vector3::new(3, 4, 12),
            ),
        };
        let right_leg_translation = Matrix4::from_translation(Vector3::new(2.0, 0.0, -12.0));
        let left_leg_translation = Matrix4::from_translation(Vector3::new(-2.0, 0.0, -12.0));
        let leg_extent = Vector3::new(4, 4, 12);

        // base

        let head_label = desc.label.map(|label| format!("{} Head", label));
        let head_desc = PartDescriptor {
            label: head_label.as_deref(),
            instance: Instance {
                transform: transform * head_translation,
            },
            extent: head_extent,
            texture_offset: Vector2::new(0, 0),
            texture_size,
        };

        let torso_label = desc.label.map(|label| format!("{} Torso", label));
        let torso_desc = PartDescriptor {
            label: torso_label.as_deref(),
            instance: Instance {
                transform: transform * torso_translation,
            },
            extent: torso_extent,
            texture_offset: Vector2::new(16, 16),
            texture_size,
        };

        let right_arm_label = desc.label.map(|label| format!("{} Right Arm", label));
        let right_arm_desc = PartDescriptor {
            label: right_arm_label.as_deref(),
            instance: Instance {
                transform: transform * right_arm_translation,
            },
            extent: arm_extent,
            texture_offset: Vector2::new(40, 16),
            texture_size,
        };

        let left_arm_label = desc.label.map(|label| format!("{} Left Arm", label));
        let left_arm_desc = PartDescriptor {
            label: left_arm_label.as_deref(),
            instance: Instance {
                transform: transform * left_arm_translation,
            },
            extent: arm_extent,
            texture_offset: Vector2::new(32, 48),
            texture_size,
        };

        let right_leg_label = desc.label.map(|label| format!("{} Right Leg", label));
        let right_leg_desc = PartDescriptor {
            label: right_leg_label.as_deref(),
            instance: Instance {
                transform: transform * right_leg_translation,
            },
            extent: leg_extent,
            texture_offset: Vector2::new(0, 16),
            texture_size,
        };

        let left_leg_label = desc.label.map(|label| format!("{} Left leg", label));
        let left_leg_desc = PartDescriptor {
            label: left_leg_label.as_deref(),
            instance: Instance {
                transform: transform * left_leg_translation,
            },
            extent: leg_extent,
            texture_offset: Vector2::new(16, 48),
            texture_size,
        };

        // overlay

        let overlay_scale = Matrix4::from_scale(1.1);

        let hat_label = desc.label.map(|label| format!("{} Hat", label));
        let hat_desc = PartDescriptor {
            label: hat_label.as_deref(),
            instance: Instance {
                transform: transform * head_translation * overlay_scale,
            },
            extent: head_extent,
            texture_offset: Vector2::new(32, 0),
            texture_size,
        };

        let jacket_label = desc.label.map(|label| format!("{} Jacket", label));
        let jacket_desc = PartDescriptor {
            label: jacket_label.as_deref(),
            instance: Instance {
                transform: transform * torso_translation * overlay_scale,
            },
            extent: torso_extent,
            texture_offset: Vector2::new(16, 32),
            texture_size,
        };

        let right_sleeve_label = desc.label.map(|label| format!("{} Right Sleeve", label));
        let right_sleeve_desc = PartDescriptor {
            label: right_sleeve_label.as_deref(),
            instance: Instance {
                transform: transform * right_arm_translation * overlay_scale,
            },
            extent: arm_extent,
            texture_offset: Vector2::new(40, 32),
            texture_size,
        };

        let left_sleeve_label = desc.label.map(|label| format!("{} Left Sleeve", label));
        let left_sleeve_desc = PartDescriptor {
            label: left_sleeve_label.as_deref(),
            instance: Instance {
                transform: transform * left_arm_translation * overlay_scale,
            },
            extent: arm_extent,
            texture_offset: match desc.kind {
                BodyKind::Classic => Vector2::new(48, 48),
                BodyKind::Slim => Vector2::new(46, 48),
            },
            texture_size,
        };

        let right_pants_leg_label = desc.label.map(|label| format!("{} Right Pants Leg", label));
        let right_pants_leg_desc = PartDescriptor {
            label: right_pants_leg_label.as_deref(),
            instance: Instance {
                transform: transform * right_leg_translation * overlay_scale,
            },
            extent: leg_extent,
            texture_offset: Vector2::new(0, 32),
            texture_size,
        };

        let left_pants_leg_label = desc.label.map(|label| format!("{} Left Pants Leg", label));
        let left_pants_leg_desc = PartDescriptor {
            label: left_pants_leg_label.as_deref(),
            instance: Instance {
                transform: transform * left_leg_translation * overlay_scale,
            },
            extent: leg_extent,
            texture_offset: Vector2::new(0, 48),
            texture_size,
        };

        Self {
            head: Part::create(device, &head_desc),
            torso: Part::create(device, &torso_desc),
            right_arm: Part::create(device, &right_arm_desc),
            left_arm: Part::create(device, &left_arm_desc),
            right_leg: Part::create(device, &right_leg_desc),
            left_leg: Part::create(device, &left_leg_desc),

            hat: Part::create(device, &hat_desc),
            jacket: Part::create(device, &jacket_desc),
            right_sleeve: Part::create(device, &right_sleeve_desc),
            left_sleeve: Part::create(device, &left_sleeve_desc),
            right_pants_leg: Part::create(device, &right_pants_leg_desc),
            left_pants_leg: Part::create(device, &left_pants_leg_desc),
        }
    }
}
