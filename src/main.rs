use {
    self::{
        camera::Camera,
        instance::Instance,
        part::{Body, BodyDescriptor, BodyKind},
        texture::{create_depth_texture, Skin},
        vertex::Vertex,
    },
    anyhow::*,
    cgmath::{Matrix4, Point3, Vector2, Vector3},
    std::{
        fs::File,
        path::PathBuf,
        time::{Duration, Instant},
    },
    structopt::StructOpt,
    wgpu::util::DeviceExt,
    winit::{
        event::{
            DeviceEvent, ElementState, Event, KeyboardInput, MouseButton, MouseScrollDelta,
            VirtualKeyCode, WindowEvent,
        },
        event_loop::{ControlFlow, EventLoop},
        window::WindowBuilder,
    },
};

mod camera;
mod instance;
mod mesh;
mod part;
mod texture;
mod vertex;

#[rustfmt::skip]
const OPENGL_TO_WGPU_MATRIX: Matrix4<f32> = Matrix4::new(
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.0, 0.0, 0.5, 1.0,
);

#[derive(Debug, Clone, StructOpt)]
struct Opt {
    /// A Minecraft skin. Must be a PNG image.
    path: PathBuf,
    /// Is the skin slim, i.e. are the arms 3 pixels wide?
    #[structopt(short, long)]
    slim: bool,
    /// Hide the overlay parts of the skin. You can also toggle the overlay by pressing H.
    #[structopt(short = "o", long)]
    hide_overlay: bool,
    /// Mouse sensitivity.
    #[structopt(short, long, default_value = "1.5")]
    mouse_sensitivity: f32,
}

#[tokio::main]
async fn main() -> Result<()> {
    let opt = Opt::from_args();

    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("skinned")
        .build(&event_loop)?;

    let instance = wgpu::Instance::new(wgpu::BackendBit::PRIMARY);
    let (size, surface) = unsafe {
        let size = window.inner_size();
        let surface = instance.create_surface(&window);
        (size, surface)
    };

    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::Default,
            compatible_surface: Some(&surface),
        })
        .await
        .unwrap();

    let optional_features = wgpu::Features::empty();
    let required_features = wgpu::Features::empty();
    let adapter_features = adapter.features();
    assert!(
        adapter_features.contains(required_features),
        "adapter does not support required features: {:?}",
        required_features - adapter_features
    );

    let needed_limits = wgpu::Limits::default();

    let (device, queue) = adapter
        .request_device(
            &wgpu::DeviceDescriptor {
                features: (optional_features & adapter_features) | required_features,
                limits: needed_limits,
                shader_validation: true,
            },
            None,
        )
        .await?;

    let mut swap_chain_desc = wgpu::SwapChainDescriptor {
        usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
        format: wgpu::TextureFormat::Bgra8UnormSrgb,
        width: size.width,
        height: size.height,
        present_mode: wgpu::PresentMode::Mailbox,
    };
    let mut swap_chain = device.create_swap_chain(&surface, &swap_chain_desc);

    let mut camera = Camera {
        eye: Point3::new(0.0, 1.0, -20.0),
        target: Point3::new(0.0, 0.0, 0.0),
        up: Vector3::unit_z(),
        aspect: swap_chain_desc.width as f32 / swap_chain_desc.height as f32,
        fovy: 70.0,
        znear: 0.1,
        zfar: 150.0,
    };

    let body_desc = BodyDescriptor {
        label: Some("Player"),
        kind: if opt.slim {
            BodyKind::Slim
        } else {
            BodyKind::Classic
        },
        ..BodyDescriptor::default()
    };
    let body = Body::create(&device, &body_desc);

    let texture_bind_group_layout =
        device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("Texture Bind Group Layout"),
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::SampledTexture {
                        multisampled: false,
                        component_type: wgpu::TextureComponentType::Float,
                        dimension: wgpu::TextureViewDimension::D2,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 1,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::Sampler { comparison: false },
                    count: None,
                },
            ],
        });
    let uniform_bind_group_layout =
        device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("Uniform Bind Group Layout"),
            entries: &[wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStage::VERTEX,
                ty: wgpu::BindingType::UniformBuffer {
                    dynamic: false,
                    min_binding_size: wgpu::BufferSize::new(64),
                },
                count: None,
            }],
        });

    let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        label: Some("Render Pipeline Layout"),
        bind_group_layouts: &[&texture_bind_group_layout, &uniform_bind_group_layout],
        push_constant_ranges: &[],
    });

    let skin = Skin::create(File::open(&opt.path)?, &device, &queue)?;
    let mut depth = create_depth_texture(&device, &swap_chain_desc);

    let mx_total = OPENGL_TO_WGPU_MATRIX * camera.matrix();
    let mx_ref: &[f32; 16] = mx_total.as_ref();
    let uniform_buf = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Uniform Buffer"),
        contents: bytemuck::cast_slice(mx_ref),
        usage: wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
    });

    let texture_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
        label: Some("Texture Bind Group"),
        layout: &texture_bind_group_layout,
        entries: &[
            wgpu::BindGroupEntry {
                binding: 0,
                resource: wgpu::BindingResource::TextureView(&skin.texture.view),
            },
            wgpu::BindGroupEntry {
                binding: 1,
                resource: wgpu::BindingResource::Sampler(&skin.texture.sampler),
            },
        ],
    });
    let uniform_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
        label: Some("Uniform Bind Group"),
        layout: &uniform_bind_group_layout,
        entries: &[wgpu::BindGroupEntry {
            binding: 0,
            resource: wgpu::BindingResource::Buffer(uniform_buf.slice(..)),
        }],
    });

    let vs_module = device.create_shader_module(wgpu::include_spirv!("shader.vert.spv"));
    let fs_module = device.create_shader_module(wgpu::include_spirv!("shader.frag.spv"));

    let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: Some("Render Pipeline"),
        layout: Some(&pipeline_layout),
        vertex_stage: wgpu::ProgrammableStageDescriptor {
            module: &vs_module,
            entry_point: "main",
        },
        fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
            module: &fs_module,
            entry_point: "main",
        }),
        rasterization_state: Some(wgpu::RasterizationStateDescriptor {
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: wgpu::CullMode::Back,
            ..Default::default()
        }),
        primitive_topology: wgpu::PrimitiveTopology::TriangleList,
        color_states: &[wgpu::ColorStateDescriptor {
            format: swap_chain_desc.format,
            color_blend: wgpu::BlendDescriptor {
                src_factor: wgpu::BlendFactor::SrcAlpha,
                dst_factor: wgpu::BlendFactor::OneMinusSrcAlpha,
                operation: wgpu::BlendOperation::Add,
            },
            alpha_blend: wgpu::BlendDescriptor::REPLACE,
            write_mask: wgpu::ColorWrite::ALL,
        }],
        depth_stencil_state: Some(wgpu::DepthStencilStateDescriptor {
            format: wgpu::TextureFormat::Depth32Float,
            depth_write_enabled: true,
            depth_compare: wgpu::CompareFunction::Less,
            stencil: wgpu::StencilStateDescriptor::default(),
        }),
        vertex_state: wgpu::VertexStateDescriptor {
            index_format: wgpu::IndexFormat::Uint16,
            vertex_buffers: &[Vertex::desc(), Instance::desc()],
        },
        sample_count: 1,
        sample_mask: !0,
        alpha_to_coverage_enabled: false,
    });

    let mut hide_overlay = opt.hide_overlay;
    let mouse_sensitivity = opt.mouse_sensitivity;

    let mut angle: f32 = 0.0;
    let mut height: f32 = 10.0;
    let mut radius: f32 = 40.0;

    let mut motion = Vector2::new(0.0, 0.0);
    let mut scroll = 0.0;
    let mut clicking = false;

    let mut last_update = Instant::now();
    event_loop.run(move |event, _, control_flow| {
        let _ = (&instance, &adapter);
        *control_flow = ControlFlow::WaitUntil(Instant::now() + Duration::from_millis(10));
        match event {
            Event::MainEventsCleared => {
                if last_update.elapsed() > Duration::from_millis(20) {
                    window.request_redraw();
                    last_update = Instant::now();
                }
            }
            Event::DeviceEvent { event, .. } => match event {
                DeviceEvent::MouseMotion { delta } => motion = delta.into(),
                DeviceEvent::MouseWheel { delta } => {
                    scroll = match delta {
                        MouseScrollDelta::LineDelta(_, delta_y) => delta_y as f64,
                        MouseScrollDelta::PixelDelta(delta) => delta.y,
                    }
                }
                _ => (),
            },
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::CloseRequested => {
                    *control_flow = ControlFlow::Exit;
                }
                WindowEvent::Resized(size) => {
                    swap_chain_desc.width = size.width;
                    swap_chain_desc.height = size.height;
                    camera.aspect = swap_chain_desc.width as f32 / swap_chain_desc.height as f32;

                    let mx_total = OPENGL_TO_WGPU_MATRIX * camera.matrix();
                    let mx_ref: &[f32; 16] = mx_total.as_ref();
                    queue.write_buffer(&uniform_buf, 0, bytemuck::cast_slice(mx_ref));

                    swap_chain = device.create_swap_chain(&surface, &swap_chain_desc);

                    depth = create_depth_texture(&device, &swap_chain_desc);
                }
                WindowEvent::MouseInput { state, button, .. } => {
                    if button == MouseButton::Left {
                        clicking = match state {
                            ElementState::Pressed => true,
                            ElementState::Released => false,
                        };
                    }
                }
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            state,
                            virtual_keycode: Some(keycode),
                            ..
                        },
                    ..
                } => {
                    if keycode == VirtualKeyCode::H && state == ElementState::Released {
                        hide_overlay = !hide_overlay;
                    }
                }
                _ => {
                    let dt = last_update.elapsed().as_secs_f32();

                    camera.eye = Point3::new(angle.cos() * radius, angle.sin() * radius, height);

                    let mx_total = OPENGL_TO_WGPU_MATRIX * camera.matrix();
                    let mx_ref: &[f32; 16] = mx_total.as_ref();
                    queue.write_buffer(&uniform_buf, 0, bytemuck::cast_slice(mx_ref));

                    if clicking {
                        angle += (-motion.x as f32) * mouse_sensitivity * dt;
                        height += (motion.y as f32) * mouse_sensitivity * 5.0 * dt;
                    }
                    radius += scroll as f32 * 100.0 * dt;

                    motion = Vector2::new(0.0, 0.0);
                    scroll = 0.0;
                }
            },
            Event::RedrawRequested(_) => {
                let frame = match swap_chain.get_current_frame() {
                    Ok(frame) => frame,
                    Err(_) => {
                        swap_chain = device.create_swap_chain(&surface, &swap_chain_desc);
                        swap_chain
                            .get_current_frame()
                            .expect("failed to acquire next swap chain texture")
                    }
                };

                let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
                    label: Some("Command Encoder"),
                });
                {
                    let mut pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                        color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                            attachment: &frame.output.view,
                            resolve_target: None,
                            ops: wgpu::Operations {
                                load: wgpu::LoadOp::Clear(wgpu::Color {
                                    r: 1.0,
                                    g: 0.2,
                                    b: 0.3,
                                    a: 1.0,
                                }),
                                store: true,
                            },
                        }],
                        depth_stencil_attachment: Some(
                            wgpu::RenderPassDepthStencilAttachmentDescriptor {
                                attachment: &depth.view,
                                depth_ops: Some(wgpu::Operations {
                                    load: wgpu::LoadOp::Clear(1.0),
                                    store: true,
                                }),
                                stencil_ops: None,
                            },
                        ),
                    });

                    pass.set_pipeline(&pipeline);
                    pass.set_bind_group(0, &texture_bind_group, &[]);
                    pass.set_bind_group(1, &uniform_bind_group, &[]);

                    // head
                    pass.set_index_buffer(body.head.mesh.index_buf.slice(..));
                    pass.set_vertex_buffer(0, body.head.mesh.vertex_buf.slice(..));
                    pass.set_vertex_buffer(1, body.head.instance_buf.slice(..));
                    pass.draw_indexed(0..body.head.mesh.index_count as u32, 0, 0..1);

                    // torso
                    pass.set_index_buffer(body.torso.mesh.index_buf.slice(..));
                    pass.set_vertex_buffer(0, body.torso.mesh.vertex_buf.slice(..));
                    pass.set_vertex_buffer(1, body.torso.instance_buf.slice(..));
                    pass.draw_indexed(0..body.torso.mesh.index_count as u32, 0, 0..1);

                    // right arm
                    pass.set_index_buffer(body.right_arm.mesh.index_buf.slice(..));
                    pass.set_vertex_buffer(0, body.right_arm.mesh.vertex_buf.slice(..));
                    pass.set_vertex_buffer(1, body.right_arm.instance_buf.slice(..));
                    pass.draw_indexed(0..body.right_arm.mesh.index_count as u32, 0, 0..1);

                    // left arm
                    pass.set_index_buffer(body.left_arm.mesh.index_buf.slice(..));
                    pass.set_vertex_buffer(0, body.left_arm.mesh.vertex_buf.slice(..));
                    pass.set_vertex_buffer(1, body.left_arm.instance_buf.slice(..));
                    pass.draw_indexed(0..body.left_arm.mesh.index_count as u32, 0, 0..1);

                    // right leg
                    pass.set_index_buffer(body.right_leg.mesh.index_buf.slice(..));
                    pass.set_vertex_buffer(0, body.right_leg.mesh.vertex_buf.slice(..));
                    pass.set_vertex_buffer(1, body.right_leg.instance_buf.slice(..));
                    pass.draw_indexed(0..body.right_leg.mesh.index_count as u32, 0, 0..1);

                    // left leg
                    pass.set_index_buffer(body.left_leg.mesh.index_buf.slice(..));
                    pass.set_vertex_buffer(0, body.left_leg.mesh.vertex_buf.slice(..));
                    pass.set_vertex_buffer(1, body.left_leg.instance_buf.slice(..));
                    pass.draw_indexed(0..body.left_leg.mesh.index_count as u32, 0, 0..1);

                    if !hide_overlay {
                        // hat
                        pass.set_index_buffer(body.hat.mesh.index_buf.slice(..));
                        pass.set_vertex_buffer(0, body.hat.mesh.vertex_buf.slice(..));
                        pass.set_vertex_buffer(1, body.hat.instance_buf.slice(..));
                        pass.draw_indexed(0..body.hat.mesh.index_count as u32, 0, 0..1);

                        // jacket
                        pass.set_index_buffer(body.jacket.mesh.index_buf.slice(..));
                        pass.set_vertex_buffer(0, body.jacket.mesh.vertex_buf.slice(..));
                        pass.set_vertex_buffer(1, body.jacket.instance_buf.slice(..));
                        pass.draw_indexed(0..body.jacket.mesh.index_count as u32, 0, 0..1);

                        // right sleeve
                        pass.set_index_buffer(body.right_sleeve.mesh.index_buf.slice(..));
                        pass.set_vertex_buffer(0, body.right_sleeve.mesh.vertex_buf.slice(..));
                        pass.set_vertex_buffer(1, body.right_sleeve.instance_buf.slice(..));
                        pass.draw_indexed(0..body.right_sleeve.mesh.index_count as u32, 0, 0..1);

                        // left sleeve
                        pass.set_index_buffer(body.left_sleeve.mesh.index_buf.slice(..));
                        pass.set_vertex_buffer(0, body.left_sleeve.mesh.vertex_buf.slice(..));
                        pass.set_vertex_buffer(1, body.left_sleeve.instance_buf.slice(..));
                        pass.draw_indexed(0..body.left_sleeve.mesh.index_count as u32, 0, 0..1);

                        // right pants leg
                        pass.set_index_buffer(body.right_pants_leg.mesh.index_buf.slice(..));
                        pass.set_vertex_buffer(0, body.right_pants_leg.mesh.vertex_buf.slice(..));
                        pass.set_vertex_buffer(1, body.right_pants_leg.instance_buf.slice(..));
                        pass.draw_indexed(0..body.right_pants_leg.mesh.index_count as u32, 0, 0..1);

                        // left pants leg
                        pass.set_index_buffer(body.left_pants_leg.mesh.index_buf.slice(..));
                        pass.set_vertex_buffer(0, body.left_pants_leg.mesh.vertex_buf.slice(..));
                        pass.set_vertex_buffer(1, body.left_pants_leg.instance_buf.slice(..));
                        pass.draw_indexed(0..body.left_pants_leg.mesh.index_count as u32, 0, 0..1);
                    }
                }

                queue.submit(Some(encoder.finish()));
            }
            _ => {}
        }
    });
}
